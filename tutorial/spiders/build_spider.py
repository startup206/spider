from scrapy.spider import Spider
from scrapy.selector import HtmlXPathSelector

class buildSpider(Spider):
    name = 'build'
    allowed_domains = ['rchgsa.ibm.com']
    start_urls = [
            "http://rchgsa.ibm.com/gsa/rchgsa/projects/e/emsol/cfs/build/"
            ]
    def parse(self,response):
        filename = response.url.split("/")[-2]
        hxs = HtmlXPathSelector(response)
        names = hxs.xpath('//body/pre/a/text()').extract()

        # skip description of name
        names = names[5:]
        open(filename,'wb').write(str(names))

