from django.contrib import admin
from funds.models import Fund

# Register your models here.
admin.site.register(Fund)
